/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(function() {
var exports = {};
exports.id = "pages/api/todo";
exports.ids = ["pages/api/todo"];
exports.modules = {

/***/ "./pages/api/todo/index.js":
/*!*********************************!*\
  !*** ./pages/api/todo/index.js ***!
  \*********************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"buildTodoPath\": function() { return /* binding */ buildTodoPath; },\n/* harmony export */   \"extractFeedback\": function() { return /* binding */ extractFeedback; }\n/* harmony export */ });\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fs */ \"fs\");\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! path */ \"path\");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_1__);\nfunction ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }\n\nfunction _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }\n\nfunction _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }\n\n\n\nfunction buildTodoPath() {\n  return path__WEBPACK_IMPORTED_MODULE_1___default().join(process.cwd(), 'data', 'todo.json');\n}\nfunction extractFeedback(filePath) {\n  const fileData = fs__WEBPACK_IMPORTED_MODULE_0___default().readFileSync(filePath);\n  const data = JSON.parse(fileData);\n  return data;\n}\n\nfunction handler(req, res) {\n  if (req.method === 'POST') {\n    const todo = req.body.data;\n    const item = req.body.item;\n\n    const newData = _objectSpread({\n      id: new Date().toISOString()\n    }, item);\n\n    const updatedArr = [newData, ...todo];\n    res.status(201).json({\n      message: 'Success!',\n      todo: updatedArr\n    });\n  } else {\n    const filePath = buildTodoPath();\n    const data = extractFeedback(filePath);\n    res.status(200).json({\n      todo: data\n    });\n  }\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (handler);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9teS10b2RvLWxpc3QvLi9wYWdlcy9hcGkvdG9kby9pbmRleC5qcz9kMzg5Il0sIm5hbWVzIjpbImJ1aWxkVG9kb1BhdGgiLCJwYXRoIiwicHJvY2VzcyIsImN3ZCIsImV4dHJhY3RGZWVkYmFjayIsImZpbGVQYXRoIiwiZmlsZURhdGEiLCJmcyIsImRhdGEiLCJKU09OIiwicGFyc2UiLCJoYW5kbGVyIiwicmVxIiwicmVzIiwibWV0aG9kIiwidG9kbyIsImJvZHkiLCJpdGVtIiwibmV3RGF0YSIsImlkIiwiRGF0ZSIsInRvSVNPU3RyaW5nIiwidXBkYXRlZEFyciIsInN0YXR1cyIsImpzb24iLCJtZXNzYWdlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBRU8sU0FBU0EsYUFBVCxHQUF5QjtBQUM1QixTQUFPQyxnREFBQSxDQUFVQyxPQUFPLENBQUNDLEdBQVIsRUFBVixFQUF5QixNQUF6QixFQUFpQyxXQUFqQyxDQUFQO0FBQ0g7QUFFTSxTQUFTQyxlQUFULENBQXlCQyxRQUF6QixFQUFtQztBQUN0QyxRQUFNQyxRQUFRLEdBQUdDLHNEQUFBLENBQWdCRixRQUFoQixDQUFqQjtBQUNBLFFBQU1HLElBQUksR0FBR0MsSUFBSSxDQUFDQyxLQUFMLENBQVdKLFFBQVgsQ0FBYjtBQUNBLFNBQU9FLElBQVA7QUFDSDs7QUFFRCxTQUFTRyxPQUFULENBQWlCQyxHQUFqQixFQUFzQkMsR0FBdEIsRUFBMkI7QUFDdkIsTUFBSUQsR0FBRyxDQUFDRSxNQUFKLEtBQWUsTUFBbkIsRUFBMkI7QUFDdkIsVUFBTUMsSUFBSSxHQUFHSCxHQUFHLENBQUNJLElBQUosQ0FBU1IsSUFBdEI7QUFDQSxVQUFNUyxJQUFJLEdBQUdMLEdBQUcsQ0FBQ0ksSUFBSixDQUFTQyxJQUF0Qjs7QUFFQSxVQUFNQyxPQUFPO0FBQ1RDLFFBQUUsRUFBRSxJQUFJQyxJQUFKLEdBQVdDLFdBQVg7QUFESyxPQUVOSixJQUZNLENBQWI7O0FBS0EsVUFBTUssVUFBVSxHQUFHLENBQ2ZKLE9BRGUsRUFFZixHQUFHSCxJQUZZLENBQW5CO0FBS0FGLE9BQUcsQ0FBQ1UsTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCO0FBQUVDLGFBQU8sRUFBRSxVQUFYO0FBQXVCVixVQUFJLEVBQUVPO0FBQTdCLEtBQXJCO0FBQ0gsR0FmRCxNQWVPO0FBQ0gsVUFBTWpCLFFBQVEsR0FBR0wsYUFBYSxFQUE5QjtBQUNBLFVBQU1RLElBQUksR0FBR0osZUFBZSxDQUFDQyxRQUFELENBQTVCO0FBQ0FRLE9BQUcsQ0FBQ1UsTUFBSixDQUFXLEdBQVgsRUFBZ0JDLElBQWhCLENBQXFCO0FBQUVULFVBQUksRUFBRVA7QUFBUixLQUFyQjtBQUNIO0FBQ0o7O0FBRUQsK0RBQWVHLE9BQWYiLCJmaWxlIjoiLi9wYWdlcy9hcGkvdG9kby9pbmRleC5qcy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBmcyBmcm9tICdmcyc7XG5pbXBvcnQgcGF0aCBmcm9tICdwYXRoJztcblxuZXhwb3J0IGZ1bmN0aW9uIGJ1aWxkVG9kb1BhdGgoKSB7XG4gICAgcmV0dXJuIHBhdGguam9pbihwcm9jZXNzLmN3ZCgpLCAnZGF0YScsICd0b2RvLmpzb24nKTtcbn1cblxuZXhwb3J0IGZ1bmN0aW9uIGV4dHJhY3RGZWVkYmFjayhmaWxlUGF0aCkge1xuICAgIGNvbnN0IGZpbGVEYXRhID0gZnMucmVhZEZpbGVTeW5jKGZpbGVQYXRoKTtcbiAgICBjb25zdCBkYXRhID0gSlNPTi5wYXJzZShmaWxlRGF0YSk7XG4gICAgcmV0dXJuIGRhdGE7XG59XG5cbmZ1bmN0aW9uIGhhbmRsZXIocmVxLCByZXMpIHtcbiAgICBpZiAocmVxLm1ldGhvZCA9PT0gJ1BPU1QnKSB7XG4gICAgICAgIGNvbnN0IHRvZG8gPSByZXEuYm9keS5kYXRhO1xuICAgICAgICBjb25zdCBpdGVtID0gcmVxLmJvZHkuaXRlbTtcblxuICAgICAgICBjb25zdCBuZXdEYXRhID0ge1xuICAgICAgICAgICAgaWQ6IG5ldyBEYXRlKCkudG9JU09TdHJpbmcoKSxcbiAgICAgICAgICAgIC4uLml0ZW1cbiAgICAgICAgfTtcblxuICAgICAgICBjb25zdCB1cGRhdGVkQXJyID0gW1xuICAgICAgICAgICAgbmV3RGF0YSxcbiAgICAgICAgICAgIC4uLnRvZG9cbiAgICAgICAgXVxuXG4gICAgICAgIHJlcy5zdGF0dXMoMjAxKS5qc29uKHsgbWVzc2FnZTogJ1N1Y2Nlc3MhJywgdG9kbzogdXBkYXRlZEFyciB9KTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBjb25zdCBmaWxlUGF0aCA9IGJ1aWxkVG9kb1BhdGgoKTtcbiAgICAgICAgY29uc3QgZGF0YSA9IGV4dHJhY3RGZWVkYmFjayhmaWxlUGF0aCk7XG4gICAgICAgIHJlcy5zdGF0dXMoMjAwKS5qc29uKHsgdG9kbzogZGF0YSB9KTtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IGhhbmRsZXI7Il0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./pages/api/todo/index.js\n");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/***/ (function(module) {

"use strict";
module.exports = require("fs");;

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/***/ (function(module) {

"use strict";
module.exports = require("path");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__("./pages/api/todo/index.js"));
module.exports = __webpack_exports__;

})();