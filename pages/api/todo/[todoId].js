function handler(req, res) {
    if (req.method === 'PATCH') {
        const todoId = req.query.todoId;

        const todos = req.body.data;
        const item = req.body.item;

        const updatedArr = todos.map((todo) => {
            if (todo.id == todoId) {
                return {
                    id: todo.id,
                    ...item
                }
            }

            return todo;
        })

        res.status(201).json({ message: 'Success!', todo: updatedArr });
    }

    if (req.method === 'DELETE') {
        const todoId = req.query.todoId;

        const todos = req.body.data;

        const updatedArr = todos.filter(todo => todo.id != todoId);

        res.status(201).json({ message: 'Success!', todo: updatedArr });
    }

    if (req.method === 'GET') {
        const todoId = req.query.todoId;
        const data = Array.isArray(req.body) ? req.body : [];

        const selectedData = data.find(
            (todo) => todo.id === todoId
        );

        res.status(200).json({ todo: selectedData });
    }

}

export default handler;