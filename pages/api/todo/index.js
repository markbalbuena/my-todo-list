import fs from 'fs';
import path from 'path';

export function buildTodoPath() {
    return path.join(process.cwd(), 'data', 'todo.json');
}

export function extractFeedback(filePath) {
    const fileData = fs.readFileSync(filePath);
    const data = JSON.parse(fileData);
    return data;
}

function handler(req, res) {
    if (req.method === 'POST') {
        const todo = req.body.data;
        const item = req.body.item;

        const newData = {
            id: new Date().toISOString(),
            ...item
        };

        const updatedArr = [
            newData,
            ...todo
        ]

        res.status(201).json({ message: 'Success!', todo: updatedArr });
    } else {
        const filePath = buildTodoPath();
        const data = extractFeedback(filePath);
        res.status(200).json({ todo: data });
    }
}

export default handler;