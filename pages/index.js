import { useState, useRef, useEffect } from 'react';
import AlertDialog from '../components/diaolog/alert-dialog';
import TodoDialog from '../components/todo/todo-dialog';
import ScrollupButton from '../components/scrollup-button';

const Index = () => {
  const [userInput, setUserInput] = useState('');
  const [loading, setLoading] = useState(true);
  const [allTodo, setAllTodo] = useState([]);
  const [todoList, setTodoList] = useState([]);
  const [todoItem, setTodoItem] = useState({});
  const [deletedialog, setDeleteDialog] = useState(false);
  const [tododialog, setTodoDialog] = useState(false);
  const [todoAction, setTodoAction] = useState('');

  const statusRef = useRef();

  useEffect(async () => {
    const { todo, error } = await fetch('/api/todo', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      }
    })
      .then((response) => response.json())

    setAllTodo(todo);
    setTodoList(todo);
    setLoading(false);
  }, []);

  if (loading) {
    return <p className='center'>Loading...</p>;
  }

  function addHandler() {
    setTodoAction("add");
    setTodoItem({
      text: "",
      status: "pending"
    });
    setTodoDialog(true);
  }

  function editHandler(data) {
    setTodoAction("edit");
    setTodoItem(data);
    console.log(data);
    setTodoDialog(true);
  }

  function deleteHandler(data) {
    setTodoItem(data);
    setDeleteDialog(true);
  }

  function filterHandler(data) {
    const selectedStatus = statusRef.current.value;

    if (selectedStatus) {
      const filterTodo = data.filter(item => item.status == selectedStatus);
      console.log(filterTodo);
      setTodoList(filterTodo);
    } else {
      setTodoList(data);
    }
  }

  function confirmDeleteHandler() {
    if (todoItem.id) {
      fetch('/api/todo/' + todoItem.id, {
        method: 'DELETE',
        body: JSON.stringify({
          data: allTodo
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => response.json())
        .then((data) => {
          setAllTodo(data.todo);
          filterHandler(data.todo);
        });
    }

    setDeleteDialog(false);
  }

  function saveTodoHandler(data) {
    if (todoAction == "edit") {
      if (todoItem.id) {
        fetch('/api/todo/' + todoItem.id, {
          method: 'PATCH',
          body: JSON.stringify({
            data: allTodo,
            item: data
          }),
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => response.json())
          .then((data) => {
            setAllTodo(data.todo);
            filterHandler(data.todo);
          });
      }
    }

    if (todoAction == "add") {
      if (data.text) {

        fetch('/api/todo', {
          method: 'POST',
          body: JSON.stringify({
            data: allTodo,
            item: data
          }),
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => response.json())
          .then((data) => {
            setAllTodo(data.todo);
            filterHandler(data.todo);
          });
      }
    }

    setTodoDialog(false);
  }

  return (
    <div className="h-100 w-full flex items-center justify-center bg-teal-lightest font-sans">
      { deletedialog ? <AlertDialog onCancel={e => setDeleteDialog(false)} onConfirm={confirmDeleteHandler} /> : ""}
      { tododialog ? <TodoDialog title="Add To-do" todo={todoItem} onCancel={e => setTodoDialog(false)} onSave={saveTodoHandler} /> : ""}
      <div className="bg-white rounded shadow p-6 m-4 w-full lg:w-3/6 lg:max-w-lg">
        <div className="flex mb-4 items-center">
          {/* <h3 className="w-full">My Todo List</h3> */}
          <div className="w-full font-bold text-xl mb-2">My Todo List</div>

          <select onChange={(e) => {
            e.preventDefault()
            filterHandler(allTodo)
          }} ref={statusRef} className="block appearance-none w-full bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="grid-state">
            <option value="">All</option>
            <option value="PENDING">Pending</option>
            <option value="ONGOING">On-going</option>
            <option value="DONE">Done</option>
          </select>

          <button onClick={(e) => {
            e.preventDefault()
            addHandler()
          }}
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold ml-5 py-2 px-2 rounded-full">
            <svg className="fill-current w-6 h-6" viewBox="0 0 24 24"><path d="M19,13H13V19H11V13H5V11H11V5H13V11H19V13Z" /></svg>
          </button>
        </div>

        <ul>
          {
            todoList.length ? todoList.map((todo, idx) => {
              return (
                <li key={idx}>
                  <div className="flex mb-4 items-center">
                    <p className={`w-full ${todo.status == "PENDING" ? "text-yellow-600" : todo.status == "ONGOING" ? "text-green-600" : "text-red-500"}`}>{todo.text}</p>
                    <button onClick={(e) => {
                      e.preventDefault()
                      editHandler(todo)
                    }}
                      className="bg-transparent mr-2 hover:bg-green-500 text-green-700 font-semibold hover:text-white py-2 px-4 border border-green-500 hover:border-transparent rounded">Update</button>
                    <button onClick={(e) => {
                      e.preventDefault()
                      deleteHandler(todo)
                    }} className="bg-transparent ml-2 hover:bg-red-500 text-red-700 font-semibold hover:text-white py-2 px-4 border border-red-500 hover:border-transparent rounded">Remove</button>
                  </div>
                </li>

              )
            }) : "No item found!"
          }
        </ul>
      </div>
      <ScrollupButton />
    </div>
  )
}

export default Index;